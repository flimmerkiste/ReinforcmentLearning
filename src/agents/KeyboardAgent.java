package agents;

import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import interfaces.Agent;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class KeyboardAgent implements Agent{
	JFrame inputFrame;
	boolean[] keys;
	
	public KeyboardAgent() {
		keys=new boolean[4];
		inputFrame=new JFrame("InputFrame");
		Dimension size=new Dimension(300, 100);
		inputFrame.setSize(size.width, size.height);
		inputFrame.setResizable(false);
		inputFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		inputFrame.getContentPane().add(new JLabel("Input here"));
		inputFrame.addKeyListener(new KeyListener() {
			@Override
			public void keyReleased(KeyEvent e) {
				if(e.getKeyCode()==KeyEvent.VK_W)keys[0]=false;
				if(e.getKeyCode()==KeyEvent.VK_A)keys[1]=false;
				if(e.getKeyCode()==KeyEvent.VK_S)keys[2]=false;
				if(e.getKeyCode()==KeyEvent.VK_D)keys[3]=false;
			}
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode()==KeyEvent.VK_W)keys[0]=true;
				if(e.getKeyCode()==KeyEvent.VK_A)keys[1]=true;
				if(e.getKeyCode()==KeyEvent.VK_S)keys[2]=true;
				if(e.getKeyCode()==KeyEvent.VK_D)keys[3]=true;
			}
			@Override
			public void keyTyped(KeyEvent e) {}
		});
		inputFrame.setVisible(true);
	}
	
	@Override
	public int step(double[] state, double reward) {
		/*int action=0;
		for(int i=0; i<keys.length; i++)action+=(keys[i]?1:0)*Math.pow(2, i);
		return action;*/
		if(keys[0])return 1;
		if(keys[1])return 2;
		return 0;
	}

}
