package agents;

import interfaces.Agent;

import org.apache.commons.math3.linear.ArrayRealVector;

import core.Main;

public class QNeuralNet implements Agent{
	final double LEARNINGRATE=1, DISCOUNT=0.5, EPS=0.01, BRAINLEARNINGRATE=1;
	Network brain;
	double minVal, scale;//(minQ,maxQ) zu (0,1) f�r NN
	
	double[] lastState;
	Action lastAction=null;
	
	
	public QNeuralNet() {
		brain=new Network(new int[]{Main.environment.getStateSize()+1,100,5,1});
		double qLim=1/(1-DISCOUNT);
		double minQ=Main.environment.getRewardRange()[0]*qLim;
		double maxQ=Main.environment.getRewardRange()[1]*qLim;
		minVal=minQ;
		scale=maxQ-minQ;
	}
	
	@Override
	public int step(double[] state, double reward) {
		
		/*Debug out
		 * for(String s:qTable.keySet()){
		 * System.out.println(qTable.entrySet().size()*Main.environment.getPossibleActions());
			System.out.print(s+"->");
			for(double d:qTable.get(s))System.out.print(d+":");
			System.out.println();
		}*/
		
		//Find best action for current state
		Action bestAction=getBestAction(state);
		
		//Update
		if(lastAction!=null){
			lastAction.qVal=brainValToQVal(brain.feedForward(lastAction.id, lastState).getEntry(0));//Restore brain
			double correctionQVal=lastAction.qVal+LEARNINGRATE*(reward+DISCOUNT*bestAction.qVal-lastAction.qVal);
			
			//System.out.println("["+lastAction.qVal+":"+correctionQVal+"]");
			//System.out.println("["+(lastAction.qVal-correctionQVal)+"]");
			brain.gradDescent(BRAINLEARNINGRATE, brain.backpropagate(new ArrayRealVector(new double[]{qValToBrainVal(correctionQVal)})));
		}
		
		//Explore
		if(Math.random()<EPS){
			lastAction=new Action((int)(Main.environment.getPossibleActions()*Math.random()),0);
			lastState=state;
			return lastAction.id;
		}
		
		//Exploit
		lastAction=bestAction;
		lastState=state;
		return lastAction.id;
	}
	
	class Action{
		int id;
		double qVal;
		public Action(int id, double qVal) {
			this.id=id;
			this.qVal=qVal;
		}
	}
	
	double brainValToQVal(double b){
		return b*scale+minVal;
	}
	
	double qValToBrainVal(double q){
		return (q-minVal)/scale;
	}
	
	Action getBestAction(double[] state){
		Action[] actions=new Action[Main.environment.getPossibleActions()];
		Action bestAct=new Action(-1, -Double.MAX_VALUE);
		for(int i=0; i<actions.length; i++){
			
			actions[i]=new Action(i, brainValToQVal(brain.feedForward(i, state).getEntry(0)));
			//System.out.println("Action #"+i+":"+actions[i].qVal);
			
			if(actions[i].qVal>bestAct.qVal){
				bestAct=actions[i];
			}else if(actions[i].qVal==bestAct.qVal&&Math.random()<0.5){//Maybe Baby
				bestAct=actions[i];
			}
		}
		for(Action a:actions)System.out.println(bestAct.qVal-a.qVal);
		return bestAct;
	}
}
