package agents;

import interfaces.Agent;

import java.util.HashMap;

import org.apache.commons.math3.linear.ArrayRealVector;

import core.Main;

public class QTable implements Agent{
	final double LEARNINGRATE=1, DISCOUNT=0.5, EPS=0.01;
	HashMap<String, double[]> qTable=new HashMap<>();
	
	String lastState;
	int lastAction=-1;
	
	
	String stateKey;//TODO: Better Hashing Method
	
	
	double[] curState;
	double minVal, scale;
	Network brain;
	public QTable() {
		brain=new Network(new int[]{Main.environment.getStateSize()+1,100,100,100,50,1});
		double qLim=1/(1-DISCOUNT);
		double minQ=Main.environment.getRewardRange()[0]*qLim;
		double maxQ=Main.environment.getRewardRange()[1]*qLim;
		minVal=minQ;
		scale=maxQ-minQ;
	}
	
	double brainValToQVal(double b){
		return b*scale+minVal;
	}
	
	double qValToBrainVal(double q){
		return (q-minVal)/scale;
	}
	
	@Override
	public int step(double[] state, double reward) {
		curState=state;
		stateKey="";
		for(double d:state)stateKey+=d;
		
		double[] qVals;
		if(qTable.containsKey(stateKey)){
			qVals=qTable.get(stateKey);
		}else{
			qTable.put(stateKey, new double[Main.environment.getPossibleActions()]);
			qVals=qTable.get(stateKey);
		}
		
		//for(double d:(double[])qTable.get(stateKey))System.out.println(d);
		
		
		/*Debug out
		 * for(String s:qTable.keySet()){
		 * System.out.println(qTable.entrySet().size()*Main.environment.getPossibleActions());
			System.out.print(s+"->");
			for(double d:qTable.get(s))System.out.print(d+":");
			System.out.println();
		}*/
		
		
		//Update
		if(lastAction!=-1){
			double oldVal=qTable.get(lastState)[lastAction];
			qTable.get(lastState)[lastAction]=oldVal+LEARNINGRATE*(reward+DISCOUNT*qVals[getBestAction(stateKey)]-oldVal);
		}
		
		//Explore
		if(Math.random()<EPS){
			lastState=stateKey;
			lastAction=(int)(Main.environment.getPossibleActions()*Math.random());
			return lastAction;
		}
		
		//Exploit
		lastState=stateKey;
		lastAction=getBestAction(stateKey);
		return lastAction;
	}
	
	int getBestAction(String state){
		double[] actions=qTable.get(state);
		double max=-Double.MAX_VALUE;
		int bestAct=-1;
		for(int i=0; i<actions.length; i++){
			brain.feedForward(i, curState);
			brain.gradDescent(1, brain.backpropagate(new ArrayRealVector(new double[]{qValToBrainVal(actions[i])})));
			if(actions[i]>max){
				max=actions[i];
				bestAct=i;
			}else if(actions[i]==max&&Math.random()<0.5){//Maybe Baby
				max=actions[i];
				bestAct=i;
			}
		}
		return bestAct;
	}

}
