package enviroments;

import java.awt.Graphics;

import interfaces.Environment;

public class RandomReward implements Environment{

	double rewards=0;
	
	@Override
	public void update(int action) {
		rewards+=Math.random()/5;
	}

	@Override
	public double getLastReward() {
		return rewards;
	}

	@Override
	public double[] getCurrentState() {
		return null;
	}

	@Override
	public void draw(Graphics g) {
	}

	@Override
	public boolean isDrawable() {
		return false;
	}
	
	@Override
	public int getStateSize() {
		return 0;
	}

	@Override
	public int getPossibleActions() {
		return 0;
	}

	@Override
	public double[] getRewardRange() {
		return null;
	}

}
