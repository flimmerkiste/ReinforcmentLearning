package core;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class InformationPanel extends JFrame{
	JCheckBox chckbxDraw;
	JSlider slider;
	JPanel screen;
	JPanel graphArea;
	Graph rewardGraph;
	private JLabel lblSps;
	private JPanel panel_1;
	JCheckBox chckbxPlot;
	private JPanel panel_2;
	private JLabel lblLastrwd;
	private JLabel lblSmoothrwd;
	private Component rigidArea;
	
	final int REWARDCACHESIZE=100;
	final int REWARDDIGITS=3;
	double[] rewards=new double[REWARDCACHESIZE];
	int index, usedIndicies;
	
	public InformationPanel() {
		setTitle("AAI");
		getContentPane().setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.SOUTH);
		
		slider = new JSlider();
		slider.setValue(42);
		panel.add(slider);
		slider.addChangeListener(new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				if(slider.getValue()==100){
					Main.sps=0;
					lblSps.setText("SPS:Inf");
				}else {
					Main.sps=Math.pow(2, slider.getValue()*0.14);
					lblSps.setText("SPS:"+Math.round(Main.sps*10)/10d);
				}
			}
		});
		
		JButton btnStart = new JButton("Start");
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Main.paused=!Main.paused;
				btnStart.setText(Main.paused?"Start":"Stop");
			}
		});
		panel.add(btnStart);
		
		lblSps = new JLabel("SPS:60");
		panel.add(lblSps);
		
		chckbxDraw = new JCheckBox("Draw");
		chckbxDraw.setSelected(true);
		panel.add(chckbxDraw);
		
		chckbxPlot = new JCheckBox("Plot");
		chckbxPlot.setSelected(true);
		panel.add(chckbxPlot);
		
		panel_1 = new JPanel();
		getContentPane().add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(new BorderLayout(0, 0));
		
		panel_2 = new JPanel();
		panel_1.add(panel_2, BorderLayout.WEST);
		panel_2.setLayout(new BoxLayout(panel_2, BoxLayout.Y_AXIS));
		
		graphArea = new JPanel(){
			@Override
			public void paint(Graphics g) {
				super.paint(g);
				if(chckbxPlot.isSelected())rewardGraph.draw(g);
			}
		};
		panel_2.add(graphArea);
		graphArea.setLayout(null);
		rewardGraph=new Graph();
		
		lblLastrwd = new JLabel("Last Reward: 0");
		lblLastrwd.setAlignmentY(Component.TOP_ALIGNMENT);
		lblLastrwd.setAlignmentX(Component.CENTER_ALIGNMENT);
		panel_2.add(lblLastrwd);
		
		rigidArea = Box.createRigidArea(new Dimension(150, 0));
		panel_2.add(rigidArea);
		
		lblSmoothrwd = new JLabel("Smooth Reward: 0");
		lblSmoothrwd.setAlignmentY(Component.TOP_ALIGNMENT);
		lblSmoothrwd.setAlignmentX(Component.CENTER_ALIGNMENT);
		panel_2.add(lblSmoothrwd);
		
		screen = new JPanel(){
			@Override
			public void paint(Graphics g) {
				super.paint(g);
				if(Main.environment.isDrawable()&chckbxDraw.isSelected())Main.environment.draw(g);
			}
		};
		screen.setBackground(Color.WHITE);
		panel_1.add(screen, BorderLayout.CENTER);
		
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(new Dimension(700, 450));
		setMinimumSize(new Dimension(500, 100));
		setLocationRelativeTo(null);
		setVisible(true);
		
		rewardGraph.setSize(graphArea.getWidth(), graphArea.getHeight());//Lets hope this works now
	}
	
	public void addReward(double rwd){
		rewards[index]=rwd;
		if(usedIndicies<100)usedIndicies++;
		index++;
		if(index>99)index=0;
		lblLastrwd.setText("Last Reward: "+Math.round(rwd*Math.pow(10, REWARDDIGITS))/Math.pow(10, REWARDDIGITS));
		lblSmoothrwd.setText("Smooth Reward: "+Math.round(getAvgReward()*Math.pow(10, REWARDDIGITS))/Math.pow(10, REWARDDIGITS));
		rewardGraph.addPoint(rwd);
		graphArea.repaint();
	}
	
	public double getAvgReward(){
		double sum=0;
		for(double d:rewards)sum+=d;
		return sum/usedIndicies;
	}
	
	public int percToX(double x){
		return (int) (x*(screen.getWidth()/100d));
	}
	
	public int percToY(double y){
		return (int) (y*(screen.getHeight()/100d));
	}
}

class Graph{
	ArrayList<Double> points=new ArrayList<>();
	int width, height, size;
	double maxBound, minBound, alp=1.2;
	
	void setSize(int width, int height) {
		this.width=width;
		this.height=height;
		size=width;//Lets try
		System.out.println(width+":"+height);
	}
	
	void addPoint(double p){
		points.add(p);
		if(points.size()>size)points.remove(0);
		double max=-Double.MAX_VALUE;
		double min=Double.MAX_VALUE;
		for(double d:points){
			if(d>max)max=d;
			if(d<min)min=d;
		}
		double mean=(max+min)/2;
		double gSize=((max-min)/2)*alp;
		maxBound=mean+gSize;
		minBound=mean-gSize;
	}
	
	double toGraphHeight(double p){
		return height-(p-minBound)*(height/(maxBound-minBound));
	}
	
	void draw(Graphics g){
		g.setColor(Color.BLACK);
		for(int i=0; i<points.size(); i++){
			try{g.drawRect(i+1, (int) toGraphHeight(points.get(i)), 1, 1);}catch(Exception e){}
		}
	}
}