package core;


import interfaces.Agent;
import interfaces.Environment;
import agents.QNeuralNet;
import agents.QTable;
import enviroments.LogicEnvironment;
import enviroments.SimplePongEnvironment;

public class Main {
	public static Agent agent;
	public static Environment environment;
	public static InformationPanel panel;
	public static boolean paused=true;
	public static double sps=60;
	public static void main(String[] args) {
		environment=new SimplePongEnvironment();
		//agent=new QNeuralNet();
		agent=new QTable();
		panel=new InformationPanel();
		mainLoop();
	}
	
	static void mainLoop(){
		environment.update(agent.step(environment.getCurrentState(), 0));
		try{
			while(true){
				while(paused)Thread.sleep(10);
				long startTime=System.currentTimeMillis();
				
				environment.update(agent.step(environment.getCurrentState(), environment.getLastReward()));
				
				if(environment.isDrawable()&&panel.chckbxDraw.isSelected())panel.screen.repaint();
				if(panel.chckbxPlot.isSelected())panel.addReward(environment.getLastReward());
					
				long deltaTime=System.currentTimeMillis()-startTime;
				long sleepTime=(long) (1000/sps-deltaTime);
				if(sps!=0&&sleepTime>0)Thread.sleep(sleepTime);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
