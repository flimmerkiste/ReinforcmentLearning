package core;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class GraphDisplay extends JFrame{
	private static final long serialVersionUID = 1L;
	public double[][] graphs;
	public double xMax, scaleX, yMin, yMax, scaleY;
	public final Dimension SIZE=new Dimension(500, 500);
	public GraphPanel graphPanel;
	public GraphDisplay(double[][] graphs, double xMax, double yMin, double yMax){
		this.graphs=graphs;
		this.xMax=xMax;
		this.yMin=yMin;
		this.yMax=yMax;
		scaleX=xMax/(double)SIZE.width;
		scaleY=Math.abs(yMax-yMin)/SIZE.height;
		graphPanel=new GraphPanel();
		setTitle("Graph Display");
		add(graphPanel);
		pack();
		setResizable(false);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	public void changexMax(double xMax){
		this.xMax=xMax;
		scaleX=xMax/(double)SIZE.width;
	}
	
	class GraphPanel extends JPanel{
		@Override
		public void paint(Graphics g) {
			super.paint(g);
			for(int i=0; i<=xMax; i+=100){
				int absX=(int)(i/scaleX);
				g.drawLine(absX, SIZE.height, absX, SIZE.height-3);
			}
			for(double i=yMin; i<yMax; i+=0.25){
				int absY=(int)(SIZE.height-(i)/scaleY);
				g.drawLine(0, absY, 3, absY);
			}
			
			for(int i=0; i<graphs.length; i+=1){
				if(i==0)g.setColor(Color.RED);
				else if(i==1)g.setColor(Color.GREEN);
				else if(i==2)g.setColor(Color.BLUE);
				double lastX=0, lastY=0;
				for(int x=0; x<graphs[i].length; x++){
					double absX=x/scaleX;
					double absY=SIZE.height-((graphs[i][x])/scaleY);
					if(x!=0)g.drawLine((int)lastX, (int)lastY, (int)absX, (int)absY);
					lastX=absX;
					lastY=absY;
				}
			}
		}
		public GraphPanel() {
			//setBackground(Color.WHITE);
			setSize(SIZE);
			setPreferredSize(SIZE);
			setMaximumSize(SIZE);
			setMinimumSize(SIZE);
		}
	}
}
